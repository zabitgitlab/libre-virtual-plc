package arch

import "fmt"

type Command interface {
	Execute()
}

type InstructionStl struct {
	Instruction   string
}

func NewInstructionStl() *InstructionStl {
	return &InstructionStl{
		Instruction:   "",
	}
}

type MakeNetworkCommand struct {
	instructionStl *InstructionStl
}

type MakeSCommand struct {
	instructionStl *InstructionStl
}

type MakeLdCommand struct {
	instructionStl *InstructionStl
}

func (c *MakeNetworkCommand) Execute() {
	//interpreta l'istruzione
	fmt.Println(c.instructionStl.Instruction," Logica comando di sezione ",)
}

func (c *MakeSCommand) Execute() {
	//interpreta l'istruzione
	fmt.Println(c.instructionStl.Instruction," Logica comando di imposta ",)
}

func (c *MakeLdCommand) Execute() {
	//interpreta l'istruzione LD
	fmt.Println(c.instructionStl.Instruction," Logica comando di caricamento ",)
}

func (is *InstructionStl) MakeNetwork(instruction string) Command {
	is.Instruction = instruction

	return &MakeNetworkCommand{
		instructionStl: is,
	}
}

func (is *InstructionStl) MakeS(instruction string) Command {
	is.Instruction = instruction

	return &MakeSCommand{
		instructionStl: is,
	}
}

func (is *InstructionStl) MakeLd(instruction string) Command {
	is.Instruction = instruction

	return &MakeLdCommand{
		instructionStl: is,
	}
}
