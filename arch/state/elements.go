package state

type Timer struct {
	T [40][4]Txx
	C [20][3]Cxx
}

type IO struct {
	Ib [2]uint8
	Qb [2]uint8
}

type Memory struct {
	Vb [100]uint8
	Vw [100]uint8
	Vd [100]uint8
	Ac [4]uint8
}

type Merker struct {
	Mm [4][2]uint8
}