package arch

import "fmt"

type ProgramAwl struct{
	Instructions []InstrInterpreter
}

func (progr ProgramAwl) isEmpty() bool{
	if len(progr.Instructions) <= 0{
		return true
	}
	//TODO controllare che nel vettore ci siano veramente le istruzioni
	return false
}

const (
	NETWORK = "NETWORK"
	LD = "LD"
	S = "S"
)
type InstrInterpreter struct {
	/*è la string: "NETWORK 1" dell'intera istruzione*/
	Text string
	/*rappresenta gli elementi dell'istruzione
	  es: NETWORK 1
	      Elements[0]: NETWORK
		  Elements[1]: 1
	*/
	Elements []string
}

type Interpreter struct {

}

func (interp Interpreter)InterpreterNetwork(instrInterp InstrInterpreter){
	fmt.Println("Interpreta ",instrInterp.Text)
}

func (interp Interpreter)InterpreterLd(instrInterp InstrInterpreter){
	fmt.Println("Interpreta ",instrInterp.Text)
}

type Expression interface {
	Interpret(interpreter Interpreter)
}

type NetworkExpression struct{
	NetInstr InstrInterpreter
}

func (netExp NetworkExpression) Interpret(interpreter Interpreter){
	interpreter.InterpreterNetwork(netExp.NetInstr)
}

type LdExpression struct{
	LdInstr InstrInterpreter
}

func (ldExp LdExpression) Interpret(interpreter Interpreter){
	interpreter.InterpreterLd(ldExp.LdInstr)
}

func isInstruction(o string) bool {
	if o == NETWORK || o == LD || o == S {
		return true
	}
	return false
}

func ParseInstr(instrI InstrInterpreter){
	var interpreter Interpreter
	var exp Expression

	//verifica se l'istruzione è valida
	if !isInstruction(instrI.Elements[0]) {
		//TO-DO implementare gli errori
		return
	}

	//preparo l'interprete dell'istruzione
	if instrI.Elements[0] == "NETWORK" {
		exp = NetworkExpression{NetInstr: instrI}
	}else if instrI.Elements[0] == "LD"{
		exp = LdExpression{LdInstr: instrI}
	}

	exp.Interpret(interpreter)
}




