package arch

import (
	"bufio"
	"os"
	"strings"
)

// read line by line into memory
// all file contents is stores in lines[]
func ReadLines(path string) ([]InstrInterpreter, error) {
	var instructions []InstrInterpreter

	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		row := scanner.Text()
		var instr InstrInterpreter
		instr.Text = row
		instr.Elements = strings.Split(instr.Text, " ")

		instructions = append(instructions, instr)
	}

	return instructions, scanner.Err()
}

