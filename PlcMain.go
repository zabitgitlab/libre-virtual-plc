package main

import (
	"fmt"
	"github.com/c-bata/go-prompt"
	"github.com/spf13/cobra"
	"libre-virtual-plc/arch"
	"libre-virtual-plc/arch/state"
	"strconv"
	"strings"
)

var program arch.ProgramAwl
var timer state.Timer
var io state.IO
var merker state.Merker
var curCursorInstr int = -2


//al plc vengono passati dei commandi da eseguire
type Plc struct {
	Commands []arch.Command
}



//il plc esegue i comandi
//( "sotto classe" Command perchè gli oggetti implementata l'interfaccia Command)
func (p Plc) executeCommands() {
	for _, c := range p.Commands {
		c.Execute()
	}
}

func load(in string){
	c := strings.Split(in, " ")

	if len(c) < 2 {
		fmt.Println("comando LOAD incompleto")
		return
	}

	//controllo che sia un file di estensione awl
	extension := ".awl"
	if !strings.Contains(c[1],extension){
		fmt.Println("[file.awl] parametro non corretto")
		return
	}

	fmt.Println("LOAD: comando eseguito.")
	//le istruzioni lette da file
	instructions, _ := arch.ReadLines("/home/zabit/go/src/libre-virtual-plc/hello-world-awl.awl")
	//#########################################################

	//creo il programma di istruzioni
	for i := 0; i < len(instructions); i++{
		program.Instructions = append(program.Instructions, instructions[i])
	}

	//setto il cursore prima dell'esecuzione del programma
	curCursorInstr = -1

}

func dumpRegisters(in string){
	c := strings.Split(in, " ")

	if len(c) > 1 {
		fmt.Println("comando DUMP-REGISTERS non accetta parametri")
		return
	}
	fmt.Println("DUMP-REGISTERS: comando eseguito.")
	fmt.Printf("I0 %08b \n", io.Ib[0])
	fmt.Printf("I1 %08b \n", io.Ib[1])
	fmt.Printf("Q0 %08b \n", io.Qb[0])
	fmt.Printf("Q1 %08b \n", io.Qb[1])
}

func run(in string){
	c := strings.Split(in, " ")

	if len(c) > 1 {
		fmt.Println("comando RUN non accetta parametri")
		return
	}

	fmt.Println("RUN: comando eseguito.")

	//il programma viene eseguito dall'inizio
	curCursorInstr = -1
	//faccio il parse delle istruzioni del programma
	var i int
	for i = 0; i < len(program.Instructions); i++{
		curCursorInstr = i
		arch.ParseInstr(program.Instructions[i])
	}

	//resetto il cursore delle istruzioni
	//in modo che lo step parta dall'inizio
	curCursorInstr = -1
	fmt.Println(i," instruzioni eseguite.")
	if i <= 0{
		fmt.Println("Prima effettua il caricamento del programma.")
	}

}

/*
	controllo lo step successivo
	reset alla prima istruzione in modo da fare lo step circolarmente
	all'inizio o alla successiva istruzione
	eseguo l'istruzione del programma
*/
func step(in string){
	c := strings.Split(in, " ")

	if len(c) > 1 {
		fmt.Println("comando STEP non accetta parametri")
		return
	}

	fmt.Println("STEP: esecuzione del comando")

	if curCursorInstr <= -2{
		fmt.Println("Prima effettua il caricamento del programma.")
		return
	}

	//
	//fmt.Println("contatore instructions: ",len(program.Instructions))
	if (curCursorInstr + 1) >= len(program.Instructions){
		//reset alla prima istruzione in modo da fare lo step circolarmente
		//fmt.Println("ciclico controllo")
		curCursorInstr = -1
	}
	//all'inizio o alla successiva istruzione
	curCursorInstr++
	//eseguo l'istruzione del programma
	arch.ParseInstr(program.Instructions[curCursorInstr])
}

func set(in string){
	c := strings.Split(in, " ")

	if len(c) < 2 {
		fmt.Println("comando SET incompleto")
		return
	}
	//il primo carattere è un I
	//il successivo carattere è 0 o 1
	//il successivo carattere è compreso tra 0 e 7

	param := []rune(c[1])
	if param[0] != 'I'{
		fmt.Println(string(param[0])," invece di 'I'. parametro errato.")
		return
	}

	str := string(param[1])
	input, _ := strconv.Atoi(str)

	if input < 0 || input > 1{
		fmt.Println(input,"invece di 0 o 1. parametro x errato.")
		return
	}

	if param[2] != ','{
		fmt.Println(string(param[2])," invece di ',' parametro errato.")
		return
	}

	str = string(param[3])
	inputByte, _ := strconv.Atoi(str)
	//Compreso tra 0 e 7
	if inputByte < 0 || inputByte > 7{
		fmt.Println(inputByte," invece di 0 a 7. parametro y errato.")
		return
	}

	var convInt [8]int
	convInt[0] = 1
	convInt[1] = 2
	convInt[2] = 4
	convInt[3] = 8
	convInt[4] = 16
	convInt[5] = 32
	convInt[6] = 64
	convInt[7] = 128

	io.Ib[input] = uint8(convInt[inputByte])
	fmt.Println("SET: comando eseguito")
}

func get(in string){
	c := strings.Split(in, " ")

	if len(c) < 2 {
		fmt.Println("comando GET incompleto")
		return
	}

	param := []rune(c[1])
	if !(param[0] == 'I' || param[0] == 'O') {
		fmt.Println(c[1],"parametro I|O non corretto. Valore accettato: I|O")
		return
	}
	if param[0] == 'I' {
		fmt.Printf("I0 %08b \n", io.Ib[0])
		fmt.Printf("I1 %08b \n", io.Ib[1])
	}else if param[0] == 'O'{
		fmt.Printf("Q0 %08b \n", io.Qb[0])
		fmt.Printf("Q1 %08b \n", io.Qb[1])
	}
}

func reset(in string){
	c := strings.Split(in, " ")

	if len(c) > 1 {
		fmt.Println("comando RESET non accetta parametri")
		return
	}

	fmt.Println("RESET: esecuzione del comando")
	//il cursore è all'inizio del programma
	curCursorInstr = -1
	//setto gli ingressi e le uscite
	io.Ib[0] = 0
	io.Ib[1] = 0

	io.Qb[0] = 0
	io.Qb[1] = 0
}

func executor(in string) {
	in = strings.TrimSpace(in)
	c := strings.Split(in, " ")

	switch c[0] {
	case "LOAD":
		load(in)
		break
	case "DUMP-REGISTERS":
		dumpRegisters(in)
		break
	case "RUN-SINGLE-CYCLE":
		run(in)
		break
	case "STEP":
		step(in)
		break
	case "SET":
		set(in)
		break
	case "GET":
		get(in)
		break
	case "RESET":
		reset(in)
		break
	default:
		break
	}
}

func completer(d prompt.Document) []prompt.Suggest {
	s := []prompt.Suggest{
		{Text: "LOAD nomefile.awl", Description: "Legge il contenuto del programma e lo carica in memoria"},
		{Text: "DUMP-REGISTERS", Description: "Scrive in output il contenuto dei registri ingressi e uscite"},
		{Text: "RUN-SINGLE-CYCLE", Description: "Esegue tutto il programma"},
		{Text: "STEP", Description: "Esegue un passo del programma"},
		{Text: "SET [Ix,y]", Description: "Imposta il bit dell'ingresso di I"},
		{Text: "GET [mem]", Description: "Scrive in output il valore di memoria I o O"},
		{Text: "RESET", Description: "resetta la simulazione da zero"},
	}
	return prompt.FilterHasPrefix(s, d.GetWordBeforeCursor(), true)
}

func main() {

	var rootCmd = &cobra.Command{Use: "libre-virtual-plc"}
	rootCmd.Execute()
	/*
	//#######################################################################
	//creo il plc
	plc := Plc{}

	//fmt.Printf("ciao ciao %d \n",arch.Sum(1,5))
	fmt.Printf("Lettura file sorgente STL(AWL) con tutte le istruzioni. \n")

	//cro tre istruzioni che verranno lette da file
	var inst [3]*arch.InstructionStl
	for i := 0; i < 3; i++ {
		inst[i] = arch.NewInstructionStl()
	}

	tasks := []arch.Command{
		inst[0].MakeNetwork("NETWORK 1"),
		inst[1].MakeLd("LD I0.0"),
		inst[2].MakeS("S Q1.0,1"),
	}

	//ciclo i tasks cosi il plc ha i tasks (le implementazioni dell'interfaccia Command)
	for i := 0; i < len(tasks); i++ {
		plc.Commands = append(plc.Commands, tasks[i])
	}

	//eseguo tutti i tasks del plc
	plc.executeCommands()
	*/

	//0 a 255
	//var value uint8 = 120
	//fmt.Printf("valore %d \n",value)
	timer = state.Timer{
		T: [40][4]state.Txx{
			{{1,1,1} , {1,1,1} , {1,1,1}, {1,1,1}},
			//continua per altri 39 righe
		},

		C: [20][3]state.Cxx{
			{{2,2,2} , {2,2,2} , {2,2,2}},
			//continua per altri 20 righe
		},
	}

	io = state.IO{
		Ib: [2]uint8{ 0, 0 },
		Qb: [2]uint8{ 0, 0 },
	}

	merker = state.Merker{
		Mm: [4][2]uint8{{1,1},{1,1},{1,1},{1,1}},
	}

	/*
	fmt.Printf("state.timer.txx[0][0].value %08b \n", timer.T[0][0].Value)
	fmt.Printf("state.timer.cxx[0][0].value %08b \n", timer.C[0][0].Value)
	fmt.Printf("state.io.Ib[0] %08b \n", io.Ib[0])
	fmt.Printf("state.io.Qb[0] %08b \n", io.Qb[0])
	fmt.Printf("state.merker.Mm[0][0] %08b \n", merker.Mm[0][0])
	*/
	p := prompt.New(
		executor,
		completer,
		prompt.OptionPrefix(">>> "),
	)
	p.Run()

}
